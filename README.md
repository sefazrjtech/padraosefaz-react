# Padrão react para sistemas internos SEFAZ-RJ em...

![react logo](http://www.developpez.com/public/images/news/react-facebook.png)

## 1. Links importantes
---
- [Landing Page do projeto](#http://lp-padraosistemas.surge.sh/)

## 2. Download do projeto
---
- O projeto foi baseado no [Create-react-app](#https://github.com/facebook/create-react-app), feito pela equipe do facebook.

1. CLONAR O PROJETO.
```sh
[ HTTPS ]
git clone https://mystrader@bitbucket.org/sefazrjtech/padraosefaz-react.git
```
2. BAIXAR AS DEPENDÊNCIAS 
```sh
npm install
```
3. RODAR O PROJETO
```sh
npm start
```
- __PRONTO__, agora é só codificar!  

## 3. Objetivo
---
Utilizar arquitetura via rest e também abrir uma janela para utilizar as melhores práticas web (webpack, cssmodule, componetização, ux melhorado, app nativo android e iphone com reactNative). O projeto serve também para estudo pessoal da tecnologia para desenvolvimento do profissional de backend e frontend.


### 4. CHANGELOG
---
### 0.0.1 *(23.03.2018)*

  - Usei o padrão de projeto próprio do facebook: [projeto create react app](#https://github.com/facebook/create-react-app)
  
### 0.0.2 *(12.04.2018)*
#### O que acrescentei e o que vou retirar.
Nestes primeiros commits, vou registrar a implentação inicial do [bootstrapSefaz](#http://padraosistemas-sefazrj.surge.sh/) para o padrão React de desenvolvimento, abaixo algumas adaptações que fiz de início.  

```html
<!--// ÁRVORE DO PROJETO-->
my-app/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    (+) componentes/  [* Onde vão ficar os futuros componentes]
    (+) assets/  [* Do bootstrap sefaz]
    (-) App.css
    (-) App.js
    (-) App.test.js
    (-) index.css
    (-) index.js
    (-) logo.svg
```
```json
{
  "name": "Projeto",
  "version": "0.0.1",
  "private": true,
  "dependencies": {
    (+) "bootstrap": "^3.3.7",
    (+) "drilldown": "^0.1.1",
    (+) "jquery": "^2.1.4",
    (+) "jquery.nicescroll": "^3.7.6",
    "react": "^16.2.0", 
    "react-bootstrap": "^0.32.1",
    "react-dom": "^16.2.0",
    "react-scripts": "1.1.1"
  },
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test --env=jsdom",
    "eject": "react-scripts eject"
  }
}
```

#### Adicionando css
Importando o css do projeto

```html
* Caminho do bootstrapSefaz:
    /padrao-sistemas-oficial\index.html
```
```html
DE :
    <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/colors.min.css" rel="stylesheet" type="text/css" />
```
```html
* Caminho no padrão ReactJs:
  \padraosefaz-react\projeto\src\index.js
```  
```javascript
PARA :
// Global stylesheets
import './assets/css/icons/icomoon/styles.css';
import './assets/css/core.min.css';
import './assets/css/components.min.css';
import './assets/css/colors.min.css';
```

#### Adicionando o Bootstrap via npm (não estou usando por enquanto)
assim temos a vantagem de injetar no componente somente o estilo do componente que queremos, como visto a baixo
```sh
λ npm install --save react-bootstrap bootstrap@3
```
```js
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
// Coloque quaisquer outras importações abaixo para que o CSS dos seus 
// componentes tenha precedência sobre os estilos padrão.
```
Temos que usar o "import" dentro do arquivo  ```src/App.js``` ou chamar dentro do seu componente.
arquivos:
```js
import { Navbar, Jumbotron, Button } from 'react-bootstrap';
```
-exemplo desta aplicação :  [`App.js`](https://gist.githubusercontent.com/gaearon/85d8c067f6af1e56277c82d19fd4da7b/raw/6158dd991b67284e9fc8d70b9d973efe87659d72/App.js)


#### Adicionando Jquery

```sh
npm install jquery@2.1.1
```
no index.js
```sh
import $ from 'jquery';
```
ou podemos no **index.js** apontar diretamente na pasta  **[node modules]** do nosso projeto. 

```javascript
import '../node_modules/jquery/dist/jquery.min.js';
```
Fiz isso também para __outras dependências__...  

```javascript
index.js (ReactJS)
import './assets/js/core/libraries/bootstrap.min.js';
import './assets/js/plugins/ui/nicescroll.min.js';
import './assets/js/plugins/ui/drilldown.js';
```
### Notas do release:
-----
> É recomendável usar a importação via [npm, package manager for  JavaScript](#https://www.npmjs.com/), fazendo a busca da sua dependência e seguindo as instruções.
---
> ainda vou manter o [registerServiceWorker](#https://developers.google.com/web/fundamentals/prime> rs/service-workers/?hl=pt-br)
-------
> Tudo o que é importado aqui ficará no bundle, mas... se as dependências foram colocadas na pasta [ project ] e importadas no index.html (padrão antigo), na hora do build da aplicação você tem que colocar manualmente as dependências.
-------


### 0.0.3 *(13.04.2018)*
- [FIX] O projeto está importante apenas o google fonts
- [FIX] Projeto rodando com jquery, mas sem o app.js (agora core.js)


### 0.0.4 *(17.04.2018)*
- [UPDATE] componente de header aceitando dos props.
- [FIX]  Retirada de libs como LESS que não será usado neste stack. (mas sim na faze pré-projeto) de prototipação.
- [FIX]  [app.js] > [core.js] / E agora a app.js se torna o js central do react. como é esperado. (não central do bootstrapSefaz)
- *Neste momento você já pode baixar o projeto para exemplos simples. sem usar todas as bibliotecas fornecidas pelo bootstrapSefaz. mas podemos trabalhar de forma essencial como a amaioria dos exemplos de estudo.
