
import React from 'react';
import ReactDOM from 'react-dom';

// Global stylesheets 


import './assets/css/icons/icomoon/styles.css';
import './assets/css/bootstrap.min.css';
import './assets/css/core.min.css';
import './assets/css/components.min.css';
import './assets/css/colors.min.css';

import './assets/js/core/libraries/bootstrap.min.js';
//import './assets/js/plugins/ui/nicescroll.min.js';
//import './assets/js/plugins/ui/drilldown.js';

 //import './core.js';

// Native library create-react-app
    import registerServiceWorker from './registerServiceWorker';

// Import components to general template
   import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
