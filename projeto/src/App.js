import React, { Component } from 'react';
import Head from './components/layout/head/head.js';

class App extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Head 
                    title="Manual Sistemas" 
                    subtitle="UX, UI e Padrão de Código de Sistemas Internos" 
                  />
            </div>
        );
    }
}
export default App;